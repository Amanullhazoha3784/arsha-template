// Back To Top Button
const backToTopBtn = document.querySelector(".back-to-top-btn");

window.addEventListener("scroll", () => {
    if (window.pageYOffset > 100) {
        backToTopBtn.classList.add("active");
    } else {
        backToTopBtn.classList.remove("active");
    }
})

// Menu Bar Scrolling Background
const headerScrolled = document.querySelector(".fixed-top");

window.addEventListener("scroll", () => {
    if (window.pageYOffset > 100) {
        headerScrolled.classList.add("header-scrolled");
    } else {
        headerScrolled.classList.remove("header-scrolled");
    }
})

// Mobile Toggler Btn
const navMenu = document.querySelector(".nav-menu");
const menuActive = document.querySelector(".active-btn");
const menuCencel = document.querySelector(".cencel-btn");

menuActive.onclick = () => {
    navMenu.classList.add("active");
    menuActive.classList.add("hide");
}

menuCencel.onclick = () => {
    navMenu.classList.remove("active");
    menuActive.classList.remove("hide");
}

/*---------asjdl;jsj
$(document).ready(function(){
    $('.item').click(function(){
        const value = $(this).attr('data-filter');
            if(value == 'all'){
                $('.item-here').show('1000');
            }
            else{
                $('.item-here').not('.'+value).hide('1000');
                $('.item-here').filter('.'+value).show('1000');
            }
    })
    $('.item').click(function(){
        $(this).addClass('active').siblings().removeClass('active');
    })
})*/

var $grid = $(".portfolio-container").isotope({
    itemSelector:'.item-here'
});
$(".item").click(function(){
    var filterValue = $(this).attr("data-filter");
    console.log(filterValue);

    $(".item").removeClass("active");
    $(this).addClass("active");

    $grid.isotope({filter:filterValue});
})

// aso animation 

AOS.init({
    duration: 1000,
    once: true
});

// venobox

$(document).ready(function(){
    $('.venobox').venobox({
        closeColor : '#fff',
        autoplay : 'true',
        titleBackground: 'rgba(40, 58, 90, 0.9)',
        titleColor: '#fff',
    }); 
});


